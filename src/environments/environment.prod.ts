import {defaultEnv} from './environment.default';

const productionEnv = {
  name: 'Production',

  production: true,

  host: ''
};
export const environment = Object.assign(defaultEnv, productionEnv);
